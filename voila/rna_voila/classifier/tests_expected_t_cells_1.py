sg_file = '/home/paul/PycharmProjects/majiq/test_cases/classifier/t_cells_1/splicegraph.sql'
psi_file = '/home/paul/PycharmProjects/majiq/test_cases/classifier/t_cells_1/ran_treg.psi.voila'

expected_modules = {
    "gene:ENSG00000266967": [
        {
            'afe': '1'
        }, {}
    ],
    "gene:ENSG00000134352": [
        {
            # 'tandem_cassette': '1'
        }, {}, {}, {}, {}, {}, {}
    ],
    "gene:ENSG00000170581": [
        {}, {}, {}, {}, {
            'tandem_cassette': ''
        }, {}
    ],
    "gene:ENSG00000177239":[
        {}, {}, {}, {}, {
            # 'exitron': "3"
        }, {}, {}
    ],
    "gene:ENSG00000205744":[
        {}, {}, {},
        {
            "mutually_exclusive": "1",
            "alternative_intron": "1"
        },
        {}
    ],
    "gene:ENSG00000003756":[
        {}, {}, {}, {}, {}, {},
        {
            # 'tandem_cassette': '1'
        },
        {}, {}
    ],
    "gene:ENSG00000164674":[
        {},
        {
            "module_id": "gene:ENSG00000164674_2",
            "mutually_exclusive": "",
            # "exitron": "1",
            "p_alt3ss": "",
            "p_alt5ss": "1",
            "alternative_intron": "2"

        }, {}, {}, {}, {}
    ],
    "gene:ENSG00000111540": [
        {
            "module_id": "gene:ENSG00000111540_1",
            "lsv_ids": "gene:ENSG00000111540:s:55974011-55974139",
            "cassette_exon": "",
            "alt3ss": "",
            "alt5ss": "",
            "p_alt3ss": "",
            "p_alt5ss": "",

            "mutually_exclusive": "",
            "ale": "1",
            "afe": "",
            "p_ale": "",
            "p_afe": "",
            "multi_exon_spanning": "",
            "alternative_intron": "1",
            "complex": "True"
        }
    ],
    "gene:ENSG00000081721": [
        {
            "module_id": "gene:ENSG00000081721_1",
            "lsv_ids": "gene:ENSG00000081721:s:161749758-161750145",
            "cassette_exon": "",
            "alt3ss": "1",
            "alt5ss": "",
            "p_alt3ss": "",
            "p_alt5ss": "",

            "mutually_exclusive": "",
            "ale": "",
            "afe": "",
            "p_ale": "",
            "p_afe": "",
            "multi_exon_spanning": "",
            "alternative_intron": "",
            "complex": "False"
        },
        {
            "module_id": "gene:ENSG00000081721_2",
            "lsv_ids": "gene:ENSG00000081721:s:161753075-161753406",
            "cassette_exon": "",
            "alt3ss": "",
            "alt5ss": "",
            "p_alt3ss": "",
            "p_alt5ss": "",

            "mutually_exclusive": "",
            "ale": "",
            "afe": "",
            "p_ale": "",
            "p_afe": "",
            "multi_exon_spanning": "",
            "alternative_intron": "1",
            "complex": "False"
        }
    ],
    "gene:ENSG00000082074": [
        {
            "module_id": "gene:ENSG00000082074_1",
            "lsv_ids": "gene:ENSG00000082074:s:39138657-39138691",
            "cassette_exon": "",
            "alt3ss": "1",
            "alt5ss": "",
            "p_alt3ss": "",
            "p_alt5ss": "",

            "mutually_exclusive": "",
            "ale": "",
            "afe": "",
            "p_ale": "",
            "p_afe": "",
            "multi_exon_spanning": "",
            "alternative_intron": "1",
            "complex": "True"
        },
        {
            "module_id": "gene:ENSG00000082074_2",
            "lsv_ids": "gene:ENSG00000082074:s:39127741-39127807;gene:ENSG00000082074:t:39124253-39124317",
            "cassette_exon": "1",
            "alt3ss": "",
            "alt5ss": "1",
            "p_alt3ss": "1",
            "p_alt5ss": "2",

            "mutually_exclusive": "",
            "ale": "",
            "afe": "",
            "p_ale": "",
            "p_afe": "",
            "multi_exon_spanning": "",
            "alternative_intron": "2",
            "complex": "True"
        },
        {
            "module_id": "gene:ENSG00000082074_3",
            "lsv_ids": "gene:ENSG00000082074:s:39118874-39119036",
            "cassette_exon": "",
            "alt3ss": "",
            "alt5ss": "",
            "p_alt3ss": "",
            "p_alt5ss": "",

            "mutually_exclusive": "",
            "ale": "",
            "afe": "",
            "p_ale": "",
            "p_afe": "",
            "multi_exon_spanning": "",
            "alternative_intron": "1",
            "complex": "False"
        }
    ],
    "gene:ENSG00000082213": [
        {
            "module_id": "gene:ENSG00000082213_1",
            "lsv_ids": "gene:ENSG00000082213:s:31535744-31535893",
            "cassette_exon": "",
            "alt3ss": "1",
            "alt5ss": "",
            "p_alt3ss": "",
            "p_alt5ss": "",

            "mutually_exclusive": "",
            "ale": "",
            "afe": "",
            "p_ale": "",
            "p_afe": "",
            "multi_exon_spanning": "",
            "alternative_intron": "",
            "complex": "False"
        },
        {
            "module_id": "gene:ENSG00000082213_2",
            "lsv_ids": "gene:ENSG00000082213:s:31545646-31545714;gene:ENSG00000082213:t:31551293-31551432",
            "cassette_exon": "1",
            "alt3ss": "",
            "alt5ss": "",
            "p_alt3ss": "",
            "p_alt5ss": "",

            "mutually_exclusive": "",
            "ale": "",
            "afe": "",
            "p_ale": "",
            "p_afe": "",
            "multi_exon_spanning": "",
            "alternative_intron": "",
            "complex": "False"
        }
    ],
    "gene:ENSG00000047249": [
        {
            "module_id": "gene:ENSG00000047249_1",
            "lsv_ids": "gene:ENSG00000047249:t:53801799-53801896;gene:ENSG00000047249:s:53811164-53811217",
            "cassette_exon": "1",
            "alt3ss": "",
            "alt5ss": "",
            "p_alt3ss": "",
            "p_alt5ss": "",

            "mutually_exclusive": "",
            "ale": "",
            "afe": "",
            "p_ale": "",
            "p_afe": "",
            "multi_exon_spanning": "",
            "alternative_intron": "",
            "complex": "False"
        },
        {
            "module_id": "gene:ENSG00000047249_2",
            "lsv_ids": "gene:ENSG00000047249:t:53769618-53769743;gene:ENSG00000047249:s:53801799-53801896",
            "cassette_exon": "",
            "alt3ss": "",
            "alt5ss": "",
            "p_alt3ss": "",
            "p_alt5ss": "",

            "mutually_exclusive": "",
            # "ale": "",
            # "afe": "",
            "p_ale": "",
            "p_afe": "",
            "multi_exon_spanning": "1",
            "alternative_intron": "",
            # "complex": "False"
        }
    ],
    "gene:ENSG00000082898": [
        {
            "module_id": "gene:ENSG00000082898_1",
            "lsv_ids": "gene:ENSG00000082898:t:61533772-61534278;gene:ENSG00000082898:s:61537562-61537694",
            "cassette_exon": "1",
            "alt3ss": "",
            "alt5ss": "",
            "p_alt3ss": "",
            "p_alt5ss": "",

            "mutually_exclusive": "",
            "ale": "",
            # "afe": "1",
            "p_ale": "",
            "p_afe": "",
            # "multi_exon_spanning": "1",
            # "alternative_intron": "1",
            # "complex": "True"
        },
        {
            "module_id": "gene:ENSG00000082898_2",
            "lsv_ids": "gene:ENSG00000082898:s:61490642-61490776",
            "cassette_exon": "",
            "alt3ss": "",
            "alt5ss": "",
            "p_alt3ss": "",
            "p_alt5ss": "",

            "mutually_exclusive": "",
            "ale": "",
            "afe": "",
            "p_ale": "",
            "p_afe": "",
            "multi_exon_spanning": "",
            "alternative_intron": "1",
            "complex": "False"
        },
        {
            "module_id": "gene:ENSG00000082898_3",
            "lsv_ids": "gene:ENSG00000082898:s:61483937-61484105",
            "cassette_exon": "",
            "alt3ss": "",
            "alt5ss": "",
            "p_alt3ss": "",
            "p_alt5ss": "",

            "mutually_exclusive": "",
            "ale": "",
            "afe": "",
            "p_ale": "",
            "p_afe": "",
            "multi_exon_spanning": "",
            "alternative_intron": "1",
            "complex": "False"
        },
        {
            "module_id": "gene:ENSG00000082898_4",
            "lsv_ids": "gene:ENSG00000082898:s:61481185-61481281",
            "cassette_exon": "",
            "alt3ss": "",
            "alt5ss": "",
            "p_alt3ss": "",
            "p_alt5ss": "",

            "mutually_exclusive": "",
            "ale": "",
            "afe": "",
            "p_ale": "",
            "p_afe": "",
            "multi_exon_spanning": "",
            "alternative_intron": "1",
            "complex": "False"
        }
    ],
    "gene:ENSG00000083168": [
        {
            "module_id": "gene:ENSG00000083168_1",
            "lsv_ids": "gene:ENSG00000083168:t:42045878-42049302;gene:ENSG00000083168:s:42051901-42052026",
            "cassette_exon": "1",
            "alt3ss": "",
            "alt5ss": "1",
            "p_alt3ss": "",
            "p_alt5ss": "",

            "mutually_exclusive": "",
            "ale": "",
            "afe": "",
            "p_ale": "",
            "p_afe": "",
            "multi_exon_spanning": "",
            "alternative_intron": "",
            "complex": "True"
        },
        {
            "module_id": "gene:ENSG00000083168_2",
            "lsv_ids": "gene:ENSG00000083168:t:41955296-41955411;gene:ENSG00000083168:s:41973404-41974734",
            "cassette_exon": "",
            "alt3ss": "",
            "alt5ss": "",
            "p_alt3ss": "",
            # "p_alt5ss": "1",

            "mutually_exclusive": "",
            "ale": "",
            "afe": "",
            "p_ale": "",
            "p_afe": "",
            "multi_exon_spanning": "1",
            "alternative_intron": "1",
            "complex": "True"
        },
        {
            "module_id": "gene:ENSG00000083168_3",
            "lsv_ids": "gene:ENSG00000083168:s:41955296-41955411",
            "cassette_exon": "",
            "alt3ss": "",
            "alt5ss": "",
            "p_alt3ss": "",
            "p_alt5ss": "",

            "mutually_exclusive": "",
            "ale": "",
            "afe": "",
            "p_ale": "",
            "p_afe": "",
            "multi_exon_spanning": "",
            "alternative_intron": "1",
            "complex": "False"
        }
    ],
    "gene:ENSG00000175582": [
        {
            "module_id": "gene:ENSG00000175582_1",
            "lsv_ids": "gene:ENSG00000175582:s:73720846-73720899;gene:ENSG00000175582:t:73716251-73716362",
            "cassette_exon": "",
            "alt3ss": "",
            "alt5ss": "",
            "p_alt3ss": "",
            "p_alt5ss": "",

            "mutually_exclusive": "1",
            "ale": "",
            "afe": "",
            "p_ale": "",
            "p_afe": "",
            "multi_exon_spanning": "",
            "alternative_intron": "",
            "complex": "False"
        }
    ],
    "gene:ENSG00000144535": [
        {
            "module_id": "gene:ENSG00000144535_1",
            "lsv_ids": "gene:ENSG00000144535:s:232087487-232087844",
            "cassette_exon": "",
            "alt3ss": "",
            "alt5ss": "",
            "p_alt3ss": "",
            "p_alt5ss": "",

            "mutually_exclusive": "",
            "ale": "1",
            "afe": "",
            "p_ale": "",
            "p_afe": "",
            "multi_exon_spanning": "",
            "alternative_intron": "",
            "complex": "False"
        },
        {
            "module_id": "gene:ENSG00000144535_2",
            "lsv_ids": "gene:ENSG00000144535:s:232163459-232163632;gene:ENSG00000144535:t:232210326-232210405",
            "cassette_exon": "1",
            "alt3ss": "",
            "alt5ss": "",
            "p_alt3ss": "",
            "p_alt5ss": "",

            "mutually_exclusive": "",
            "ale": "",
            "afe": "",
            "p_ale": "",
            "p_afe": "",
            "multi_exon_spanning": "",
            "alternative_intron": "",
            "complex": "False"
        },
        {
            "module_id": "gene:ENSG00000144535_3",
            "lsv_ids": "gene:ENSG00000144535:s:232330690-232330776;gene:ENSG00000144535:t:232333840-232333987",
            "cassette_exon": "1",
            "alt3ss": "",
            "alt5ss": "",
            "p_alt3ss": "",
            "p_alt5ss": "",

            "mutually_exclusive": "",
            "ale": "",
            "afe": "",
            "p_ale": "",
            "p_afe": "",
            "multi_exon_spanning": "",
            "alternative_intron": "",
            "complex": "False"
        }
    ],
    "gene:ENSG00000111653": [
        {
            "module_id": "gene:ENSG00000111653_1",
            "lsv_ids": "gene:ENSG00000111653:s:6663065-6663148;gene:ENSG00000111653:t:6653230-6653397;gene:ENSG00000111653:s:6656438-6656798",
            "cassette_exon": "3",
            "alt3ss": "",
            "alt5ss": "",
            "p_alt3ss": "",
            "p_alt5ss": "",

            "mutually_exclusive": "",
            "ale": "",
            "afe": "",
            "p_ale": "",
            "p_afe": "",
            "multi_exon_spanning": "2",
            "alternative_intron": "",
            "complex": "True"
        },
        {
            "module_id": "gene:ENSG00000111653_2",
            "lsv_ids": "gene:ENSG00000111653:s:6652936-6653050",
            "cassette_exon": "",
            "alt3ss": "",
            "alt5ss": "1",
            "p_alt3ss": "",
            "p_alt5ss": "",

            "mutually_exclusive": "",
            "ale": "",
            "afe": "",
            "p_ale": "",
            "p_afe": "",
            "multi_exon_spanning": "",
            "alternative_intron": "",
            "complex": "False"
        },
        {
            "module_id": "gene:ENSG00000111653_3",
            "lsv_ids": "gene:ENSG00000111653:s:6652662-6652770",
            "cassette_exon": "",
            "alt3ss": "",
            "alt5ss": "",
            "p_alt3ss": "",
            "p_alt5ss": "",

            "mutually_exclusive": "",
            "ale": "1",
            "afe": "",
            "p_ale": "",
            "p_afe": "",
            "multi_exon_spanning": "",
            "alternative_intron": "",
            "complex": "False"
        }
    ],
    "gene:ENSG00000285708": [
        {
            "module_id": "gene:ENSG00000285708_1",
            "lsv_ids": "gene:ENSG00000285708:s:71710412-71710484",
            "cassette_exon": "",
            "alt3ss": "",
            "alt5ss": "",
            "p_alt3ss": "",
            "p_alt5ss": "",

            "mutually_exclusive": "",
            "ale": "",
            "afe": "",
            "p_ale": "",
            "p_afe": "",
            "multi_exon_spanning": "",
            "alternative_intron": "1",
            "complex": "False"
        },
        {
            "module_id": "gene:ENSG00000285708_2",
            "lsv_ids": "gene:ENSG00000285708:s:71693875-71693941",
            "cassette_exon": "",
            "alt3ss": "",
            "alt5ss": "",
            "p_alt3ss": "",
            "p_alt5ss": "",

            "mutually_exclusive": "",
            "ale": "",
            "afe": "",
            "p_ale": "",
            "p_afe": "",
            "multi_exon_spanning": "",
            "alternative_intron": "1",
            "complex": "False"
        },
        {
            "module_id": "gene:ENSG00000285708_3",
            "lsv_ids": "gene:ENSG00000285708:t:71359150-71359244",
            "cassette_exon": "",
            "alt3ss": "",
            "alt5ss": "",
            "p_alt3ss": "",
            "p_alt5ss": "",

            "mutually_exclusive": "",
            "ale": "",
            "afe": "1",
            "p_ale": "",
            "p_afe": "",
            "multi_exon_spanning": "",
            "alternative_intron": "",
            "complex": "False"
        },
        {
            "module_id": "gene:ENSG00000285708_4",
            "lsv_ids": "gene:ENSG00000285708:s:71112536-71112637",
            "cassette_exon": "",
            "alt3ss": "",
            "alt5ss": "",
            "p_alt3ss": "",
            "p_alt5ss": "",

            "mutually_exclusive": "",
            "ale": "",
            "afe": "",
            "p_ale": "1",
            "p_afe": "",
            "multi_exon_spanning": "",
            "alternative_intron": "",
            "complex": "False"
        },
        {
            "module_id": "gene:ENSG00000285708_5",
            "lsv_ids": "gene:ENSG00000285708:s:70977828-70978029",
            "cassette_exon": "",
            "alt3ss": "1",
            "alt5ss": "",
            "p_alt3ss": "",
            "p_alt5ss": "",

            "mutually_exclusive": "",
            "ale": "",
            "afe": "",
            "p_ale": "",
            "p_afe": "",
            "multi_exon_spanning": "",
            "alternative_intron": "",
            "complex": "False"
        },
        {
            "module_id": "gene:ENSG00000285708_6",
            "lsv_ids": "gene:ENSG00000285708:s:70976941-70977042",
            "cassette_exon": "",
            "alt3ss": "",
            "alt5ss": "",
            "p_alt3ss": "",
            "p_alt5ss": "",

            "mutually_exclusive": "",
            "ale": "",
            "afe": "",
            "p_ale": "",
            "p_afe": "",
            "multi_exon_spanning": "",
            "alternative_intron": "1",
            "complex": "False"
        },
        {
            "module_id": "gene:ENSG00000285708_7",
            "lsv_ids": "gene:ENSG00000285708:s:70972555-70972676",
            "cassette_exon": "",
            "alt3ss": "",
            "alt5ss": "",
            "p_alt3ss": "",
            "p_alt5ss": "",

            "mutually_exclusive": "",
            "ale": "",
            "afe": "",
            "p_ale": "",
            "p_afe": "",
            "multi_exon_spanning": "",
            "alternative_intron": "1",
            "complex": "False"
        },
        {
            "module_id": "gene:ENSG00000285708_8",
            "lsv_ids": "gene:ENSG00000285708:s:70965890-70966056",
            "cassette_exon": "",
            "alt3ss": "",
            "alt5ss": "",
            "p_alt3ss": "",
            "p_alt5ss": "",

            "mutually_exclusive": "",
            "ale": "",
            "afe": "",
            "p_ale": "",
            "p_afe": "",
            "multi_exon_spanning": "",
            "alternative_intron": "1",
            "complex": "False"
        }
    ],
    "gene:ENSG00000151327": [
        {
            "module_id": "gene:ENSG00000151327_1",
            "lsv_ids": "gene:ENSG00000151327:t:35050033-35050396;gene:ENSG00000151327:t:35053200-35053451;gene:ENSG00000151327:s:35046453-35046628;gene:ENSG00000151327:s:35046681-35047084",
            "cassette_exon": "",
            "alt3ss": "",
            "alt5ss": "1",
            "p_alt3ss": "",
            "p_alt5ss": "",

            "mutually_exclusive": "",
            "ale": "1",
            "afe": "2",
            "p_ale": "",
            "p_afe": "",
            "multi_exon_spanning": "2",
            "alternative_intron": "2",
            "complex": "True"
        },
        {
            "module_id": "gene:ENSG00000151327_2",
            "lsv_ids": "gene:ENSG00000151327:s:35053200-35053451",
            "cassette_exon": "",
            "alt3ss": "",
            "alt5ss": "",
            "p_alt3ss": "",
            "p_alt5ss": "",

            "mutually_exclusive": "",
            "ale": "",
            "afe": "",
            "p_ale": "1",
            "p_afe": "",
            "multi_exon_spanning": "",
            "alternative_intron": "",
            "complex": "False"
        }
    ],
    "gene:ENSG00000144381": [
        {
            "module_id": "gene:ENSG00000144381_1",
            "lsv_ids": "gene:ENSG00000144381:t:197498674-197498850",
            "cassette_exon": "",
            "alt3ss": "",
            "alt5ss": "",
            "p_alt3ss": "",
            "p_alt5ss": "",

            "mutually_exclusive": "",
            "ale": "",
            "afe": "2",
            "p_ale": "",
            "p_afe": "",
            "multi_exon_spanning": "1",
            "alternative_intron": "",
            "complex": "True"
        },
        {
            "module_id": "gene:ENSG00000144381_2",
            "lsv_ids": "gene:ENSG00000144381:s:197496891-197497507",
            "cassette_exon": "",
            "alt3ss": "",
            "alt5ss": "",
            "p_alt3ss": "",
            "p_alt5ss": "",

            "mutually_exclusive": "",
            "ale": "",
            "afe": "",
            "p_ale": "",
            "p_afe": "",
            "multi_exon_spanning": "",
            "alternative_intron": "1",
            "complex": "False"
        },
        {
            "module_id": "gene:ENSG00000144381_3",
            "lsv_ids": "gene:ENSG00000144381:s:197495051-197495376",
            "cassette_exon": "",
            "alt3ss": "",
            "alt5ss": "",
            "p_alt3ss": "",
            "p_alt5ss": "",

            "mutually_exclusive": "",
            "ale": "",
            "afe": "",
            "p_ale": "",
            "p_afe": "",
            "multi_exon_spanning": "",
            "alternative_intron": "1",
            "complex": "False"
        },
        {
            "module_id": "gene:ENSG00000144381_4",
            "lsv_ids": "gene:ENSG00000144381:s:197493320-197493492",
            "cassette_exon": "",
            "alt3ss": "",
            "alt5ss": "",
            "p_alt3ss": "",
            "p_alt5ss": "",

            "mutually_exclusive": "",
            "ale": "",
            "afe": "",
            "p_ale": "1",
            "p_afe": "",
            "multi_exon_spanning": "",
            "alternative_intron": "1",
            "complex": "True"
        },
        {
            "module_id": "gene:ENSG00000144381_5",
            "lsv_ids": "gene:ENSG00000144381:s:197490197-197490572",
            "cassette_exon": "",
            "alt3ss": "",
            "alt5ss": "",
            "p_alt3ss": "",
            "p_alt5ss": "",

            "mutually_exclusive": "",
            "ale": "",
            "afe": "",
            "p_ale": "",
            "p_afe": "",
            "multi_exon_spanning": "",
            "alternative_intron": "1",
            "complex": "False"
        },
        {
            "module_id": "gene:ENSG00000144381_6",
            "lsv_ids": "gene:ENSG00000144381:s:197487858-197488036",
            "cassette_exon": "",
            "alt3ss": "",
            "alt5ss": "",
            "p_alt3ss": "",
            "p_alt5ss": "",

            "mutually_exclusive": "",
            "ale": "",
            "afe": "",
            "p_ale": "",
            "p_afe": "",
            "multi_exon_spanning": "",
            "alternative_intron": "1",
            "complex": "False"
        }
    ],
    "gene:ENSG00000090863": [
        {
            "module_id": "gene:ENSG00000090863_1",
            "lsv_ids": "gene:ENSG00000090863:t:74508839-74508925;gene:ENSG00000090863:s:74606657-74606827",
            "cassette_exon": "1",
            "alt3ss": "",
            "alt5ss": "",
            "p_alt3ss": "",
            "p_alt5ss": "",

            "mutually_exclusive": "",
            "ale": "",
            "afe": "",
            "p_ale": "",
            "p_afe": "",
            "multi_exon_spanning": "",
            "alternative_intron": "",
            "complex": "False"
        },
        {
            "module_id": "gene:ENSG00000090863_2",
            "lsv_ids": "gene:ENSG00000090863:t:74472349-74472492;gene:ENSG00000090863:s:74474172-74474632",
            "cassette_exon": "",
            "alt3ss": "",
            "alt5ss": "",
            "p_alt3ss": "",
            "p_alt5ss": "1",

            "mutually_exclusive": "",
            "ale": "",
            "afe": "",
            "p_ale": "1",
            "p_afe": "",
            "multi_exon_spanning": "",
            "alternative_intron": "1",
            "complex": "True"
        },
        {
            "module_id": "gene:ENSG00000090863_3",
            "lsv_ids": "gene:ENSG00000090863:s:74472349-74472492",
            "cassette_exon": "",
            "alt3ss": "",
            "alt5ss": "",
            "p_alt3ss": "",
            "p_alt5ss": "",

            "mutually_exclusive": "",
            "ale": "",
            "afe": "",
            "p_ale": "",
            "p_afe": "",
            "multi_exon_spanning": "",
            "alternative_intron": "1",
            "complex": "False"
        },
        {
            "module_id": "gene:ENSG00000090863_4",
            "lsv_ids": "gene:ENSG00000090863:t:74467756-74467848;gene:ENSG00000090863:s:74469985-74470073",
            "cassette_exon": "1",
            "alt3ss": "",
            "alt5ss": "",
            "p_alt3ss": "",
            "p_alt5ss": "1",

            "mutually_exclusive": "",
            "ale": "",
            "afe": "",
            "p_ale": "",
            "p_afe": "",
            "multi_exon_spanning": "",
            "alternative_intron": "1",
            "complex": "True"
        },
        {
            "module_id": "gene:ENSG00000090863_5",
            "lsv_ids": "gene:ENSG00000090863:s:74465676-74465813",
            "cassette_exon": "",
            "alt3ss": "",
            "alt5ss": "",
            "p_alt3ss": "",
            "p_alt5ss": "",

            "mutually_exclusive": "",
            "ale": "",
            "afe": "",
            "p_ale": "",
            "p_afe": "",
            "multi_exon_spanning": "",
            "alternative_intron": "1",
            "complex": "False"
        }
    ],
    "gene:ENSG00000160201": [
        {
            "module_id": "gene:ENSG00000160201_1",
            "lsv_ids": "gene:ENSG00000160201:t:43104315-43105252;gene:ENSG00000160201:s:43107451-43107587",
            "cassette_exon": "1",
            "alt3ss": "",
            "alt5ss": "",
            "p_alt3ss": "",
            "p_alt5ss": "",

            "mutually_exclusive": "",
            "ale": "",
            "afe": "",
            "p_ale": "",
            "p_afe": "1",
            "multi_exon_spanning": "",
            "alternative_intron": "",
            "complex": "True"
        },
        {
            "module_id": "gene:ENSG00000160201_2",
            "lsv_ids": "gene:ENSG00000160201:s:43104315-43105252;gene:ENSG00000160201:s:43101366-43101907;gene:ENSG00000160201:t:43097622-43100519",
            "cassette_exon": "1",
            "alt3ss": "",
            "alt5ss": "",
            "p_alt3ss": "",
            "p_alt5ss": "",

            "mutually_exclusive": "",
            "ale": "",
            "afe": "",
            "p_ale": "",
            "p_afe": "",
            "multi_exon_spanning": "",
            "alternative_intron": "",
            "complex": "False"
        },
        {
            "module_id": "gene:ENSG00000160201_3",
            "lsv_ids": "gene:ENSG00000160201:t:43095694-43096390",
            "cassette_exon": "",
            "alt3ss": "",
            "alt5ss": "",
            "p_alt3ss": "",
            "p_alt5ss": "",

            "mutually_exclusive": "",
            "ale": "",
            "afe": "",
            "p_ale": "",
            "p_afe": "",
            "multi_exon_spanning": "",
            "alternative_intron": "1",
            "complex": "False"
        },
        {
            "module_id": "gene:ENSG00000160201_4",
            "lsv_ids": "gene:ENSG00000160201:s:43095694-43096390;gene:ENSG00000160201:t:43094655-43094788",
            "cassette_exon": "1",
            "alt3ss": "",
            "alt5ss": "",
            "p_alt3ss": "1",
            "p_alt5ss": "1",

            "mutually_exclusive": "",
            "ale": "",
            "afe": "",
            "p_ale": "",
            "p_afe": "",
            "multi_exon_spanning": "",
            "alternative_intron": "2",
            "complex": "True"
        }
    ],
    "gene:ENSG00000081307": [
        {
            "module_id": "gene:ENSG00000081307_1",
            "lsv_ids": "gene:ENSG00000081307:t:132667258-132668927;gene:ENSG00000081307:s:132660601-132660698;gene:ENSG00000081307:t:132665739-132665868",
            "cassette_exon": "",
            "alt3ss": "",
            "alt5ss": "",
            "p_alt3ss": "",
            "p_alt5ss": "",

            "mutually_exclusive": "",
            "ale": "",
            "afe": "1",
            "p_ale": "",
            "p_afe": "",
            "multi_exon_spanning": "1",
            "alternative_intron": "2",
            "complex": "True"
        },
        {
            "module_id": "gene:ENSG00000081307_2",
            "lsv_ids": "gene:ENSG00000081307:t:132675605-132675680;gene:ENSG00000081307:s:132672050-132672177",
            "cassette_exon": "1",
            "alt3ss": "",
            "alt5ss": "",
            "p_alt3ss": "",
            "p_alt5ss": "1",

            "mutually_exclusive": "",
            "ale": "",
            "afe": "",
            "p_ale": "",
            "p_afe": "",
            "multi_exon_spanning": "",
            "alternative_intron": "1",
            "complex": "True"
        }
    ],
    "gene:ENSG00000081665": [
        {
            "module_id": "gene:ENSG00000081665_1",
            "lsv_ids": "gene:ENSG00000081665:t:19806942-19807068;gene:ENSG00000081665:s:19821601-19821751",
            "cassette_exon": "",
            "alt3ss": "",
            "alt5ss": "",
            "p_alt3ss": "",
            "p_alt5ss": "",

            "mutually_exclusive": "",
            "ale": "1",
            "afe": "",
            "p_ale": "",
            "p_afe": "",
            "multi_exon_spanning": "",
            "alternative_intron": "",
            "complex": "False"
        },
        {
            "module_id": "gene:ENSG00000081665_2",
            "lsv_ids": "gene:ENSG00000081665:t:19792411-19792484;gene:ENSG00000081665:s:19806942-19807068",
            "cassette_exon": "1",
            "alt3ss": "",
            "alt5ss": "",
            "p_alt3ss": "1",
            "p_alt5ss": "",

            "mutually_exclusive": "",
            "ale": "1",
            "afe": "1",
            "p_ale": "",
            "p_afe": "",
            "multi_exon_spanning": "3",
            "alternative_intron": "3",
            "complex": "True"
        },
        {
            "module_id": "gene:ENSG00000081665_3",
            "lsv_ids": "gene:ENSG00000081665:s:19791562-19791731",
            "cassette_exon": "",
            "alt3ss": "",
            "alt5ss": "",
            "p_alt3ss": "",
            "p_alt5ss": "",

            "mutually_exclusive": "",
            "ale": "",
            "afe": "",
            "p_ale": "",
            "p_afe": "",
            "multi_exon_spanning": "",
            "alternative_intron": "2",
            "complex": "True"
        }
    ],
    "gene:ENSG00000111412": [
        {
            "module_id": "gene:ENSG00000111412_1",
            "lsv_ids": "gene:ENSG00000111412:t:116710185-116717893;gene:ENSG00000111412:t:116719763-116720127",
            "cassette_exon": "3",
            "alt3ss": "",
            "alt5ss": "",
            "p_alt3ss": "",
            "p_alt5ss": "",

            "mutually_exclusive": "",
            "ale": "",
            "afe": "",
            "p_ale": "",
            "p_afe": "",
            "multi_exon_spanning": "2",
            "alternative_intron": "",
            "complex": "True"
        }
    ],
    "gene:ENSG00000162735": [
        {
            "module_id": "gene:ENSG00000162735_1",
            "lsv_ids": "gene:ENSG00000162735:t:160282826-160283109;gene:ENSG00000162735:s:160285055-160285154",
            "cassette_exon": "1",
            "alt3ss": "",
            "alt5ss": "",
            "p_alt3ss": "",
            "p_alt5ss": "",

            "mutually_exclusive": "",
            "ale": "",
            "afe": "",
            "p_ale": "",
            "p_afe": "",
            "multi_exon_spanning": "",
            "alternative_intron": "",
            "complex": "False"
        },
        {
            "module_id": "gene:ENSG00000162735_2",
            "lsv_ids": "gene:ENSG00000162735:s:160282826-160283109;gene:ENSG00000162735:t:160282039-160282200",
            "cassette_exon": "1",
            "alt3ss": "",
            "alt5ss": "",
            "p_alt3ss": "",
            "p_alt5ss": "",

            "mutually_exclusive": "",
            "ale": "",
            "afe": "",
            "p_ale": "",
            "p_afe": "",
            "multi_exon_spanning": "",
            "alternative_intron": "",
            "complex": "False"
        }
    ],
    "gene:ENSG00000161956": [
        {
            "module_id": "gene:ENSG00000161956_1",
            "lsv_ids": "gene:ENSG00000161956:s:7561875-7562263",
            "cassette_exon": "",
            "alt3ss": "",
            "alt5ss": "",
            "p_alt3ss": "",
            "p_alt5ss": "",

            "mutually_exclusive": "",
            "ale": "",
            "afe": "",
            "p_ale": "",
            "p_afe": "",
            "multi_exon_spanning": "",
            "alternative_intron": "1",
            "complex": "False"
        },
        {
            "module_id": "gene:ENSG00000161956_2",
            "lsv_ids": "gene:ENSG00000161956:s:7565695-7566051",
            "cassette_exon": "",
            "alt3ss": "",
            "alt5ss": "",
            "p_alt3ss": "",
            "p_alt5ss": "",

            "mutually_exclusive": "",
            "ale": "",
            "afe": "",
            "p_ale": "",
            "p_afe": "",
            "multi_exon_spanning": "",
            "alternative_intron": "1",
            "complex": "False"
        },
        {
            "module_id": "gene:ENSG00000161956_3",
            "lsv_ids": "gene:ENSG00000161956:s:7566927-7567004",
            "cassette_exon": "",
            "alt3ss": "",
            "alt5ss": "",
            "p_alt3ss": "",
            "p_alt5ss": "",

            "mutually_exclusive": "",
            "ale": "",
            "afe": "",
            "p_ale": "",
            "p_afe": "",
            "multi_exon_spanning": "",
            "alternative_intron": "1",
            "complex": "False"
        }
    ],
    "gene:ENSG00000119397": [
        {}, {}, {},
        {
            "module_id": "gene:ENSG00000119397_4",
            "lsv_ids": "gene:ENSG00000119397:s:121157470-121157600;gene:ENSG00000119397:t:121158586-121159019",
            # "tandem_cassette": "1",
            "multi_exon_spanning": "1",
            "complex": "False"
        }, {}, {}, {}, {}
    ],
    'gene:ENSG00000114388': [
        {
            "module_id": "gene:ENSG00000114388_1",
            "cassette_exon": "",
            "alt3ss": "1",
            "alt5ss": "1",
            "alt3and5ss": "1",
            "p_alt3ss": "",
            "p_alt5ss": "",

            "mutually_exclusive": "",
            "ale": "",
            "afe": "",
            "p_ale": "",
            "p_afe": "",
            "multi_exon_spanning": "",
            "alternative_intron": "",
            "complex": "True"
        },
        {}
    ],
    'gene:ENSG00000236449': [
        {
            "module_id": "gene:ENSG00000236449_1",
            "orphan_junction": "1",
            "p_ale": "",
            "p_afe": "",
        },
        {
            "module_id": "gene:ENSG00000236449_2",
            "orphan_junction": "1",
            "p_ale": "2",
            "p_afe": "",
        }
    ],
    'gene:ENSG00000233175': [
        {
            "module_id": "gene:ENSG00000233175_1",
            # "exitron": "2"
        },

    ],
    'gene:ENSG00000218739': [
        {},

    ],
    "gene:ENSG00000122390":[
        {}, {}, {}, {}, {}
    ],
    "gene:ENSG00000087206":[
        {},{},{},{},{},{}
    ],
    "gene:ENSG00000100796":[

    ],
    "gene:ENSG00000124151":[

    ]

}

expected_modules_constitutive = {
    'gene:ENSG00000218739': [
        {}, {}, {}, {}, {
            'constitutive_intron': '1'
        }

    ],
    'gene:ENSG00000273189': [
        {}, {
            'constitutive_junction': '1'
        }, {
            'constitutive_junction': '1'
        }, {}, {
            'constitutive_junction': '1'
        }, {
            'constitutive_junction': '1'
        }, {
            'constitutive_junction': '1'
        }
    ]
}

expected_cassette_exons = {
    'gene:ENSG00000138073': [
        {
            'ran_treg_mean_psi': '0.125',
        },{
            'ran_treg_mean_psi': '0.875',
        },{
            'ran_treg_mean_psi': '0.07913184',
        },{
            'ran_treg_mean_psi': '0.90108526',
        }
    ]
}

expected_alternative_intron = {
    'gene:ENSG00000123146': [
        {'reference_exon_coord': '14381156-14381545'},
        {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}
    ]
}

expected_alt3ss = {
    "gene:ENSG00000100796":[
        {'ran_treg_mean_psi': '0.992'},
        {'ran_treg_mean_psi': '0.008'}
    ],
    "gene:ENSG00000124151":[
        {'ran_treg_mean_psi': '0.86'},
        {'ran_treg_mean_psi': '0.14'},
        {'ran_treg_mean_psi': '0.665'},
        {'ran_treg_mean_psi': '0.335'}
    ]
}

expected_alt5ss = {
    "gene:ENSG00000122390":[
        {'ran_treg_mean_psi': '0.391'},
        {'ran_treg_mean_psi': '0.609'}
    ],
    "gene:ENSG00000087206":[
        {'ran_treg_mean_psi': '0.039'},
        {'ran_treg_mean_psi': '0.961'}
    ]
}

# expected_alt5ss = {
#     'gene:ENSG00000138085'
# }