What's new
==========

2.3.0
~~~~~

- This documentation interface was crated!
- Majiq / Voila heterogen mode and visualization: heatmap for every LSV , n by n experiment comparisons
- Voila modulizer feature, quantify and categorize specific splice graph structures
- Voila filter, split, and join utility modes to subset data or split out processing to multiple machines.
- Voila: Ability to save a displayed page as a static html file! 🎉 (heavily requested feature)
- Voila lsv instant filtering on index page in DeltaPsi mode.