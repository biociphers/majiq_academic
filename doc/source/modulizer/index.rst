###############
Modulizer Index
###############


.. toctree::
   :maxdepth: 3


   quick-start
   how-modulizer-works
   output
   splicegraph-modules
   event-types
   additional-modes
   examples

