Quick Start
===========

Please visit https://majiq.biociphers.org/ to obtain download and
installation instructions.

+----------------------------------------------------------------------+
| .. rubric:: voila categorize <splicegraph.sql> <voila file(s)> -d    |
|    <output directory> -j <threads>                                   |
|    :name: voila-c                                                    |
| ategorize-splicegraph.sql-voila-files--d-output-directory--j-threads |
+----------------------------------------------------------------------+
