.. _command-builder:

Command Builder
===============


Even with our documentation, it may be tedious to construct the applicable commands and config file
required to run majiq and voila
manually. In order to help you get started with some basic use cases, we provide this quick-to-use
interactive tool to
help you create the appropriate sequence of commands for your use case.

.. raw:: html
   :file: static/command_builder.html


