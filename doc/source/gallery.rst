Gallery
=======

Here's a list of examples of how to use MAJIQ.

.. toctree::
   :maxdepth: 1

   gallery/heterogen-vignette.ipynb
